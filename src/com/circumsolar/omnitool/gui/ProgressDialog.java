package com.circumsolar.omnitool.gui;

import com.circumsolar.omnitool.framework.Plugin;
import com.circumsolar.omnitool.framework.PluginListener;

import javax.swing.*;
import javax.swing.plaf.DimensionUIResource;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Date;

public class ProgressDialog extends JFrame {

    private JTextPane logTextPane = new JTextPane();
    private JProgressBar progressBar = new JProgressBar(0, 100);

    private Plugin plugin;
    private Thread pluginThread;

    public ProgressDialog(Plugin plugin) {

        this.plugin = plugin;

        setTitle(plugin.getName());

        logTextPane.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                    appendToPane("\n", Color.BLACK);
                    String line = getLine();
                    if (line.endsWith("\n")) {
                        line = line.substring(0, line.length() - 1);
                    }
                    plugin.sendInput(line);
                    e.consume();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        JScrollPane scrollPane = new JScrollPane(logTextPane);
        scrollPane.setPreferredSize(new DimensionUIResource(640, 600));
        add(scrollPane);

        JButton button = new JButton("Restart");
        button.addActionListener(e -> startPlugin());

        JPanel panel = new JPanel(new GridLayout(2, 0));
        panel.add(button);

        progressBar.setIndeterminate(true);
        panel.add(progressBar);

        add(panel, BorderLayout.SOUTH);

        pack();

        WindowAdapter windowAdapter = new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                super.windowClosed(e);
                plugin.removeListener(pluginListener);
            }
        };

        addWindowListener(windowAdapter);
    }

    public void start() {
        setVisible(true);
        plugin.addListener(pluginListener);
        startPlugin();
    }

    private void startPlugin() {
        pluginThread = new Thread(() -> {
            appendInfo("Starting...");
            plugin.execute();
            appendInfo("Finished.");
            progressBar.setIndeterminate(false);
            progressBar.setValue(0);
            progressBar.setValue(progressBar.getMaximum());
        });
        pluginThread.start();
    }

    private String getLine() {
        // Get section element
        Element section = logTextPane.getDocument().getDefaultRootElement();

        // Get number of paragraphs.
        // In a text pane, a span of characters terminated by single
        // newline is typically called a paragraph.
        int paraCount = section.getElementCount();

        int position = logTextPane.getCaret().getDot();

        // Get index ranges for each paragraph
        for (int i = 0; i < paraCount; i++)
        {
            Element e1 = section.getElement(i);

            int rangeStart = e1.getStartOffset();
            int rangeEnd = e1.getEndOffset();

            try
            {
                String para = logTextPane.getText(rangeStart, rangeEnd-rangeStart);

                if (position >= rangeStart && position <= rangeEnd)
                    return para;
            }
            catch (BadLocationException ex)
            {
                System.err.println("Get current line from editor error: " + ex.getMessage());
            }
        }
        return null;
    }

    private void appendMessage(String message, Color color) {
        appendToPane(new Date().toString() + ": " + message, color);
        appendToPane("\n", Color.BLACK);
    }

    private void appendMessage(String message) {
        appendMessage(message, Color.BLUE);
    }

    private void appendError(Exception e) {
        appendMessage(e.getLocalizedMessage(), Color.RED);
    }

    private void appendInfo(String message) {
        appendMessage(message, Color.magenta);
    }

    private void appendToPane(String msg, Color c) {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = logTextPane.getDocument().getLength();
        logTextPane.setCaretPosition(len);
        logTextPane.setCharacterAttributes(aset, false);
        logTextPane.replaceSelection(msg);

        logTextPane.setCaretPosition(logTextPane.getDocument().getLength());
    }

    private PluginListener pluginListener = new PluginListener() {
        @Override
        public void reportProgress(int progress) {
            if (progress < 0) {
                progressBar.setIndeterminate(true);
            } else {
                progressBar.setIndeterminate(false);
                progressBar.setValue(progress);
            }
        }

        @Override
        public void reportStatus(String message) {
            appendMessage(message);
        }

        @Override
        public void reportError(Exception error) {
            appendError(error);
        }
    };
}
