package com.circumsolar.omnitool.gui;

import javax.swing.*;
import javax.swing.plaf.DimensionUIResource;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TextResultDialog extends JFrame {

    JTextArea textArea;
    JLabel label;

    public TextResultDialog() {
        textArea = new JTextArea();
        textArea.setPreferredSize(new DimensionUIResource(320, 200));
        textArea.setLineWrap(true);
        JScrollPane scrollPane = new JScrollPane(textArea);
        add(scrollPane, BorderLayout.CENTER);
        JPanel panel = new JPanel(new GridLayout(2, 0));
        label = new JLabel();
        panel.add(label);
        JButton button = new JButton("Copy");
        button.addActionListener(e -> copyToClipboard());
        panel.add(button);
        add(panel, BorderLayout.SOUTH);
        pack();
    }

    public void setResult(String result) {
        textArea.setText(result);
        label.setText("Characters: " + result.length() + ", lines: " + result.split("\n").length);
    }

    private void copyToClipboard() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        StringSelection strSel = new StringSelection(textArea.getText());
        clipboard.setContents(strSel, null);
    }
}
