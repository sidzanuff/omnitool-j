package com.circumsolar.omnitool.gui;


import com.circumsolar.omnitool.framework.*;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class MainWindow extends JFrame implements ActionListener, KeyListener {

    private final static String CONF_FILE = "omnitool.json";

    private final ParamTableModel paramsModel;
    private final JTable paramsTable;
    private final JComboBox<String> pluginsBox;
    private final JButton executeButton;

    private List<Plugin> plugins;
    private Config config;

    public MainWindow() {
        super("OMNITOOL");

        paramsModel = new ParamTableModel();
        paramsTable = new JTable(paramsModel);
        pluginsBox = new JComboBox<>();
        executeButton = new JButton("Execute (F5)");

        paramsTable.setPreferredScrollableViewportSize(new Dimension(640, 480));
        paramsTable.setFillsViewportHeight(true);
        paramsTable.setDragEnabled(true);
        paramsTable.setTransferHandler(transferHandler);
        paramsTable.addKeyListener(this);

        executeButton.addActionListener(this);
        pluginsBox.addActionListener(this);

        JScrollPane scrollPane = new JScrollPane(paramsTable);
        add(scrollPane, BorderLayout.CENTER);

        JPanel bottomPanel = new JPanel(new GridLayout(2, 0));

        bottomPanel.add(pluginsBox);
        bottomPanel.add(executeButton);

        add(bottomPanel, BorderLayout.SOUTH);

        loadPlugins();
        loadConfig();
    }

    TransferHandler transferHandler = new TransferHandler() {
        @Override
        public boolean canImport(TransferSupport support) {
            return true;
        }

        @Override
        public boolean importData(TransferSupport support) {
            if (!support.isDrop() || paramsTable.getSelectedRowCount() == 0) {
                return false;
            }

            Transferable t = support.getTransferable();
            List<File> data;
            try {
                //noinspection unchecked
                data = (List<File>) t.getTransferData(DataFlavor.javaFileListFlavor);
            } catch (Exception e) {
                return false;
            }

//            String[] oldValues = ((String) paramsModel.getValueAt(paramsTable.getSelectedRow(), 1)).split(",");
//            List<String> values = new ArrayList<>(Arrays.asList(oldValues));
//            data.stream().map(File::getAbsolutePath).forEach(values::add);
//            String newValues = values.stream().distinct().filter(StringUtils::isNotBlank)
//                    .collect(Collectors.joining(","));

            String newValues = data.stream().map(File::getAbsolutePath).collect(Collectors.joining(","));

            paramsModel.setValueAt(newValues, paramsTable.getSelectedRow(), 1);
            paramsTable.addNotify();

            return true;
        }
    };

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == executeButton) {
            execute();
        } else if (e.getSource() == pluginsBox) {
            if (config == null) return;
            config.selectedPlugin = (String) pluginsBox.getSelectedItem();
            paramsModel.setParams(plugins.get(pluginsBox.getSelectedIndex()).getParams());
            saveConfig();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            if (paramsTable.getSelectedRowCount() > 0) {
                paramsModel.setValueAt("", paramsTable.getSelectedRow(), 1);
                paramsTable.addNotify();
            }
        } else if (e.getKeyCode() == KeyEvent.VK_F5) {
            execute();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    private void execute() {
        saveConfig();
        ProgressDialog progressDialog = new ProgressDialog(plugins.get(pluginsBox.getSelectedIndex()));
        progressDialog.start();
    }

    private void loadPlugins() {
        plugins = new ArrayList<>();

        for (Class pluginClass : new Reflections().getSubTypesOf(Plugin.class)) {
            Plugin plugin;
            try {
                plugin = (Plugin) pluginClass.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
                continue;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                continue;
            }
            plugins.add(plugin);
        }

        Collections.sort(plugins, (o1, o2) -> o1.getName().compareTo(o2.getName()));

        for (Plugin plugin : plugins) {
            pluginsBox.addItem(plugin.getName());
        }
    }

    private void loadConfig() {
        File configFile = new File(CONF_FILE);
        if (configFile.exists()) {
            try (InputStream stream = new FileInputStream(configFile);
                 Reader reader = new InputStreamReader(stream)) {

                config = new Gson().fromJson(reader, Config.class);

            } catch (IOException e) {
                e.printStackTrace();
                config = new Config();
            }
        } else {
            config = new Config();
        }

        for (Plugin plugin : plugins) {
            Optional<PluginConfig> optional = config.pluginConfigs.stream()
                    .filter(c -> c.name.equals(plugin.getName())).findFirst();
            PluginConfig pluginConfig = optional.isPresent() ? optional.get() : null;

            if (pluginConfig == null) {
                pluginConfig = new PluginConfig();
                pluginConfig.name = plugin.getName();
                config.pluginConfigs.add(pluginConfig);
            } else {
                for (Param param : plugin.getParams()) {
                    Optional<Param> paramOptional = pluginConfig.params.stream()
                            .filter(p -> p.name.equals(param.name)).findFirst();
                    param.value = paramOptional.isPresent() ? paramOptional.get().value : "";
                }
            }
            pluginConfig.params = plugin.getParams();
        }

        if (StringUtils.isNotBlank(config.selectedPlugin)) {
            pluginsBox.setSelectedItem(config.selectedPlugin);
        } else {
            pluginsBox.setSelectedIndex(0);
        }
    }

    private void saveConfig() {
        File configFile = new File(CONF_FILE);
        if (configFile.exists()) {
            //noinspection ResultOfMethodCallIgnored
            configFile.delete();
        }

        try (OutputStream stream = new FileOutputStream(configFile);
             OutputStreamWriter writer = new OutputStreamWriter(stream)) {
            new Gson().toJson(config, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createAndShowGUI() {
        MainWindow frame = new MainWindow();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(MainWindow::createAndShowGUI);
    }
}
