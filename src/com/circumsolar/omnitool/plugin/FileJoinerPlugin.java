package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;

import java.io.*;

public class FileJoinerPlugin extends Plugin {

    public static final String PARAM_INPUT_FILES = "input files";
    public static final String PARAM_OUTPUT_FILE = "output file";

    public FileJoinerPlugin() {
        super(PARAM_INPUT_FILES, PARAM_OUTPUT_FILE);
    }

    @Override
    public String getName() {
        return "File Joiner";
    }

    @Override
    public void execute() {
        super.execute();

        try {
            joinFiles();
        } catch (IOException e) {
            e.printStackTrace();
            reportError(e);
        }
    }

    public void joinFiles() throws IOException {

        byte[] buffer = new byte[4096];
        int count;

        try (OutputStream outputStream = new FileOutputStream(getParamValue(PARAM_OUTPUT_FILE))) {
            for (String inputFile : getParamValues(PARAM_INPUT_FILES)) {
                reportStatus("Joining with " + inputFile);
                try (InputStream inputStream = new FileInputStream(inputFile)) {
                    while((count = inputStream.read(buffer)) > 0) {
                        outputStream.write(buffer, 0, count);
                    }
                }
            }
            outputStream.flush();
        }
    }
}
