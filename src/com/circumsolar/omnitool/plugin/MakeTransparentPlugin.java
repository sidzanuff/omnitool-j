package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;

public class MakeTransparentPlugin extends Plugin {

    public static final String PARAM_FILES = "files";

    public MakeTransparentPlugin() {
        super(PARAM_FILES);
    }

    @Override
    public String getName() {
        return "Make transparent";
    }

    @Override
    public void execute() {
        super.execute();

        for (String fileName : getParamValues(PARAM_FILES)) {
            try {
                File file = new File(fileName);
                BufferedImage source = ImageIO.read(file);
                int color = source.getRGB(0, 0);
                Image image = makeColorTransparent(source, new Color(color));
                BufferedImage transparent = imageToBufferedImage(image);
                ImageIO.write(transparent, "PNG", file);
            } catch (IOException e) {
                e.printStackTrace();
                reportError(e);
            }
        }
    }

    private static BufferedImage imageToBufferedImage(Image image) {

        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = bufferedImage.createGraphics();
        g2.drawImage(image, 0, 0, null);
        g2.dispose();

        return bufferedImage;

    }

    public static Image makeColorTransparent(BufferedImage im, final Color color) {
        ImageFilter filter = new RGBImageFilter() {

            // the color we are looking for... Alpha bits are set to opaque
            public int markerRGB = color.getRGB() | 0xFF000000;

            public final int filterRGB(int x, int y, int rgb) {
                if ((rgb | 0xFF000000) == markerRGB) {
                    // Mark the alpha bits as zero - transparent
                    return 0x00FFFFFF & rgb;
                } else{
                    // nothing to do
                    return rgb;
                }
            }
        };

        ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(ip);
    }
}
