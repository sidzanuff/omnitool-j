package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;
import com.google.common.base.Joiner;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringExtractorPlugin extends Plugin {

    public static final String PATH_DIRECTORY = "directory";
    public static final String PARAM_EXTENSIONS = "extensions";
    public static final String PARAM_REGEX = "regex";
    public static final String PARAM_REPORT_FILE = "report file";

    public StringExtractorPlugin() {
        super(
                PATH_DIRECTORY,
                PARAM_EXTENSIONS,
                PARAM_REGEX,
                PARAM_REPORT_FILE
        );
    }

    @Override
    public String getName() {
        return "String Extractor";
    }

    @Override
    public void execute() {
        super.execute();

        File directory = new File(getParamValue(PATH_DIRECTORY));

        String[] extensions;
        if (hasParam(PARAM_EXTENSIONS)) {
            extensions = getParamValues(PARAM_EXTENSIONS);
        } else {
            extensions = null;
        }

        Pattern pattern = Pattern.compile(getParamValue(PARAM_REGEX)); //"([\"'])(?:(?=(\\\\?))\\2.)*?\\1"

        StringBuilder report = new StringBuilder();

        for (Object o : FileUtils.listFiles(directory, extensions, true)) {

            if (isCancelled()) {
                break;
            }

            File file = (File) o;
            String content;

            try {
                content = readFile(file);
            } catch (IOException e) {
                e.printStackTrace();
                reportError(new Exception("Could not read file " + file.getAbsolutePath() + ": "
                        + e.getLocalizedMessage()));
                continue;
            }

            Matcher matcher = pattern.matcher(content);

            while (matcher.find()) {

                int line = getLine(content, matcher.start());

                reportStatus(file.getName() + " (" + line + "): " + matcher.group());

                report.append(Joiner.on(",").join(new String[]{
                        file.getAbsolutePath(),
                        String.valueOf(matcher.start()),
                        String.valueOf(matcher.end()),
                        String.valueOf(line),
                        matcher.group()
                }));
                report.append('\n');
            }
        }

        if (hasParam(PARAM_REPORT_FILE)) {
            try {
                saveFile(getParamValue(PARAM_REPORT_FILE), report.toString());
            } catch (IOException e) {
                e.printStackTrace();
                reportError(e);
            }
        }
    }

    private static String readFile(File file) throws IOException {

        StringBuilder sb = new StringBuilder();

        try (FileInputStream fileInputStream = new FileInputStream(file)) {

            Reader reader = new InputStreamReader(fileInputStream, "UTF-8");
            int character = 0;

            while ((character = reader.read()) != -1) {
                sb.append((char) character);
            }

        }

        return sb.toString();
    }

    private static int getLine(String data, int start) {
        int line = 1;
        Pattern pattern = Pattern.compile("\n");
        Matcher matcher = pattern.matcher(data);
        matcher.region(0, start);
        while (matcher.find()) {
            line++;
        }
        return (line);
    }

    private static void saveFile(String fileName, String content) throws IOException {
        try (OutputStream stream = new FileOutputStream(fileName);
             OutputStreamWriter writer = new OutputStreamWriter(stream, "UTF-8")) {
            writer.write(content);
        }
    }
}
