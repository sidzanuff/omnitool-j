package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;
import com.circumsolar.util.ImageUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

@SuppressWarnings("UnusedDeclaration")
public class TileMapPlugin extends Plugin {

    public static final String PARAM_PATHS = "paths",
            PARAM_EXT = "extensions",
            PARAM_TILEWIDTH = "tilewidth",
            PARAM_TILEHEIGHT = "tileheight",
            PARAM_ROWS = "rows",
            PARAM_COLS = "columns",
            PARAM_MODE = "mode",
            PARAM_OUT = "out";

    public TileMapPlugin() {
        super(PARAM_PATHS, PARAM_EXT, PARAM_TILEWIDTH, PARAM_TILEHEIGHT, PARAM_ROWS, PARAM_COLS, PARAM_MODE, PARAM_OUT);
    }

    public String getName() {
        return "Tile map generator";
    }

    public void execute() {
        super.execute();

        doWork();
    }

    private void doWork() {

        String[] files = getParamValues(PARAM_PATHS);

        int tileWidth = getParamIntValue(PARAM_TILEWIDTH);
        int tileHeight = getParamIntValue(PARAM_TILEHEIGHT);
        int rows = getParamIntValue(PARAM_ROWS);
        int columns = getParamIntValue(PARAM_COLS);

        if (tileWidth == 0 || tileHeight == 0) {
            int maxTileWidth = 0;
            int maxTileHeight = 0;

            for (int i = 0; i < files.length; i++) {
                if (isCancelled()) {
                    return;
                }

                reportProgress(i * 100 / files.length);
                reportStatus("Measuring: " + files[i]);

                BufferedImage tile = null;
                try {
                    tile = ImageIO.read(new File(files[i]));
                } catch (IOException e) {
                    e.printStackTrace();
                    reportError(e);
                    continue;
                }

                maxTileWidth = Math.max(maxTileWidth, tile.getWidth());
                maxTileHeight = Math.max(maxTileHeight, tile.getHeight());
            }

            if (tileWidth == 0) {
                tileWidth = maxTileWidth;
            }
            if (tileHeight == 0) {
                tileHeight = maxTileHeight;
            }
        }

        if (rows == 0) {
            if (columns == 0) {
                columns = 1;
            }
            rows = (int) Math.ceil(files.length / (float) columns);
        }

        if (columns == 0) {
            columns = (int) Math.ceil(files.length / (float) rows);
        }

        Dimension tileSize = new Dimension(tileWidth, tileHeight);

        BufferedImage tileMap = new BufferedImage(tileWidth * columns, tileHeight * rows, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics = tileMap.createGraphics();

        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        graphics.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

        graphics.setComposite(AlphaComposite.Clear);
        graphics.clearRect(0, 0, tileMap.getWidth(), tileMap.getHeight());
        graphics.setComposite(AlphaComposite.SrcOver);

        int row = 0;
        int col = 0;

        for (int i = 0; i < files.length; i++) {
            if (isCancelled()) {
                return;
            }

            reportProgress(i * 100 / files.length);
            reportStatus("Drawing: " + files[i]);

            BufferedImage tile;
            try {
                tile = ImageIO.read(new File(files[i]));
            } catch (IOException e) {
                e.printStackTrace();
                reportError(e);
                continue;
            }

            Dimension srcSize;
            Point srcOffset;
            Dimension destSize;
            Point destOffset;

            String mode = getParamValue(PARAM_MODE);

            if (Objects.equals(mode, "fit")) {
                srcSize = new Dimension(tile.getWidth(), tile.getHeight());
                srcOffset = new Point();
                destSize = ImageUtils.rescale(srcSize, tileSize);
                destOffset = new Point(
                        (int) (tileWidth * col + (tileWidth - destSize.getWidth()) / 2),
                        (int) (tileHeight * row + (tileHeight - destSize.getHeight()) / 2));
            } else if (Objects.equals(mode, "crop")) {
                srcSize = ImageUtils.rescale(tileSize, new Dimension(tile.getWidth(), tile.getHeight()));
                srcOffset = new Point((int) ((tile.getWidth() - srcSize.getWidth()) / 2), (int) ((tile.getHeight() - srcSize.getHeight()) / 2));
                destSize = tileSize;
                destOffset = new Point(tileWidth * col, tileHeight * row);
            } else {
                destSize = new Dimension(tile.getWidth(), tile.getHeight());
                srcOffset = new Point();
                srcSize = new Dimension(tile.getWidth(), tile.getHeight());
                destOffset = new Point(
                        (int) (tileWidth * col + (tileWidth - destSize.getWidth()) / 2),
                        (int) (tileHeight * row + (tileHeight - destSize.getHeight()) / 2));
            }

//            graphics.DrawImage(tile,
//                    new Rectangle(destOffset, destSize),
//                    new Rectangle(srcOffset, srcSize),
//                    GraphicsUnit.Pixel);

//            graphics.drawImage(
//                    tile,
//                    destOffset.x,
//                    destOffset.y,
//                    destOffset.x + (int) destSize.getWidth(),
//                    destOffset.y + (int) destSize.getHeight(),
//                    srcOffset.x,
//                    srcOffset.y,
//                    srcOffset.x + (int) srcSize.getWidth(),
//                    srcOffset.y + (int) srcSize.getHeight(),
//                    null);

            graphics.drawImage(
                    ImageUtils.getScaledInstance(
                            tile,
                            (int) destSize.getWidth(),
                            (int) destSize.getHeight(),
                            RenderingHints.VALUE_INTERPOLATION_BICUBIC,
                            true),
                    destOffset.x,
                    destOffset.y,
                    (int)destSize.getWidth(),
                    (int)destSize.getHeight(),
                    null);

            if (++col >= columns) {
                col = 0;
                row++;

                if (row >= rows) {
                    //TODO: draw next page
                    break;
                }
            }
        }

        reportProgress(100);
        reportStatus("Saving tile map");

        try {
            ImageIO.write(tileMap, "png", new File(getParamValue(PARAM_OUT)));
        } catch (IOException e) {
            e.printStackTrace();
            reportError(e);
        }
    }
}