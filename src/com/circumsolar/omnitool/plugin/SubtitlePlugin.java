package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;
import com.circumsolar.subtitles.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.File;
import java.io.IOException;
import java.util.List;

@SuppressWarnings("UnusedDeclaration")
public class SubtitlePlugin extends Plugin {

    public static final String PARAM_INPUT_FILE = "input file";
    public static final String PARAM_INPUT_FORMAT = "input format";
    public static final String PARAM_INPUT_CHARSET = "input charset";
    public static final String PARAM_INPUT_FPS = "input fps";
    public static final String PARAM_OUTPUT_FILE = "output file";
    public static final String PARAM_OUTPUT_FOLDER = "output folder";
    public static final String PARAM_OUTPUT_FORMAT = "output format";
    public static final String PARAM_OUTPUT_CHARSET = "output charset";
    public static final String PARAM_OUTPUT_FPS = "output fps";
    public static final String PARAM_OUTPUT_OFFSET = "output offset";

    private static final String DEFAULT_CHARSET = "UTF-8";

    public SubtitlePlugin() {
        super(
                PARAM_INPUT_FILE,
                PARAM_INPUT_FORMAT,
                PARAM_INPUT_CHARSET,
                PARAM_INPUT_FPS,
                PARAM_OUTPUT_FILE,
                PARAM_OUTPUT_FOLDER,
                PARAM_OUTPUT_FORMAT,
                PARAM_OUTPUT_CHARSET,
                PARAM_OUTPUT_FPS,
                PARAM_OUTPUT_OFFSET);
    }

    @Override
    public String getName() {
        return "Subtitle converter";
    }

    @Override
    public void execute() {
        super.execute();

        Subtitles input = instantiateSubs(getParamValue(PARAM_INPUT_FORMAT));
        input.setFps(getParamFloatValue(PARAM_INPUT_FPS));
        input.setCharsetName(getParamValue(PARAM_INPUT_CHARSET, DEFAULT_CHARSET));

        Subtitles output = instantiateSubs(getParamValue(PARAM_OUTPUT_FORMAT));
        output.setFps(getParamFloatValue(PARAM_OUTPUT_FPS));
        output.setCharsetName(getParamValue(PARAM_OUTPUT_CHARSET, DEFAULT_CHARSET));

        float offset = getParamFloatValue(PARAM_OUTPUT_OFFSET);

        for (String subtitlesFile : getParamValues(PARAM_INPUT_FILE)) {
            try {

                input.read(subtitlesFile);
                List<Subtitle> subtitles = input.getSubtitles();

                for (Subtitle subtitle : subtitles) {
                    subtitle.startTime += offset;
                    subtitle.endTime += offset;
                }

                output.setSubtitles(input.getSubtitles());

                if (hasParam(PARAM_OUTPUT_FILE)) {
                    output.write(getParamValue(PARAM_OUTPUT_FILE));
                } else if (hasParam(PARAM_OUTPUT_FOLDER)) {
                    File file = new File(subtitlesFile);
                    output.write(new File(getParamValue(PARAM_OUTPUT_FOLDER), file.getName()).getAbsolutePath());
                }
            } catch (IOException e) {
                reportError(e);
            }
        }
    }

    private Subtitles instantiateSubs(String format) {
        if (format.equals("mdvd")) {
            return new MdvdSubtitles();
        }

        if (format.equals("subrip")) {
            return new SubRipSubtitles();
        }

        if (format.equals("tmp")) {
            return new TmpSubtitles();
        }

        throw new NotImplementedException();
    }
}
