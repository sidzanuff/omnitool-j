package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;
import com.circumsolar.util.ImageUtils;
import sun.nio.ch.IOUtil;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class ImageResizerPlugin extends Plugin {

    public static final String PARAM_IMAGES = "images";
    public static final String PARAM_WIDTH = "width";
    public static final String PARAM_HEIGHT = "height";
    public static final String PARAM_MODE = "mode";
    public static final String PARAM_OUTPUT_FOLDER = "output folder";

    public ImageResizerPlugin() {
        super(
                PARAM_IMAGES,
                PARAM_WIDTH,
                PARAM_HEIGHT,
                PARAM_MODE,
                PARAM_OUTPUT_FOLDER
        );
    }

    @Override
    public String getName() {
        return "Image Resizer";
    }

    @Override
    public void execute() {
        super.execute();

        int width = getParamIntValue(PARAM_WIDTH);
        int height = getParamIntValue(PARAM_HEIGHT);

        int mode = -1;

        if (getParamValue(PARAM_MODE).equals("fit")) {
            mode = ImageUtils.SCALE_MODE_FIT;
        } else if (getParamValue(PARAM_MODE).equals("crop")) {
            mode = ImageUtils.SCALE_MODE_CROP;
        }

        String[] paths = getParamValues(PARAM_IMAGES);

        for (int i = 0; i < paths.length; i++) {

            reportProgress(i * 100 / paths.length);
            reportStatus("Resising image " + paths[i]);

            try {
                File imageFile = new File(paths[i]);
                File outputFile = new File(getParamValue(PARAM_OUTPUT_FOLDER), imageFile.getName());
                BufferedImage input = ImageIO.read(imageFile);
                BufferedImage output = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
                Graphics2D graphics = (Graphics2D) output.getGraphics();
                ImageUtils.scaleImage(graphics, input, new Dimension(width, height), mode);
                ImageIO.write(output, "png", outputFile);
            }catch(Exception e) {
                reportError(e);
            }
        }
    }
}
