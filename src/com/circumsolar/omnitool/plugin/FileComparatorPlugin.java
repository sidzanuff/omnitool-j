package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class FileComparatorPlugin extends Plugin {

    public static final String PARAM_FILE1 = "file1";
    public static final String PARAM_FILE2 = "file2";

    public FileComparatorPlugin() {
        super(PARAM_FILE1, PARAM_FILE2);
    }

    @Override
    public String getName() {
        return "File Comparator";
    }

    @Override
    public void execute() {
        super.execute();

        try {
            if (FileUtils.contentEquals(new File(getParamValue(PARAM_FILE1)), new File(getParamValue(PARAM_FILE2)))) {
                reportStatus("Files identical");
            } else {
                reportStatus("Files different");
            }
        } catch (IOException e) {
            e.printStackTrace();
            reportError(e);
        }
    }
}
