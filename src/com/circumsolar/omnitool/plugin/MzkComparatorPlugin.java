package com.circumsolar.omnitool.plugin;

import com.circumsolar.mzkbb.*;
import com.circumsolar.omnitool.framework.Plugin;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class MzkComparatorPlugin extends Plugin {

    public static final String PARAM_FILE1 = "file1";
    public static final String PARAM_FILE2 = "file2";

    private MzkDb db1;
    private MzkDb db2;

    public MzkComparatorPlugin() {
        super(PARAM_FILE1, PARAM_FILE2);
    }

    @Override
    public String getName() {
        return "MZK BB Comparator";
    }

    @Override
    public void execute() {
        super.execute();

        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
            reportError(e);
            return;
        }

        try {
            //compareUwagi();
            compareDatabases();
        } catch (SQLException e) {
            e.printStackTrace();
            reportError(e);
        }

        close();
    }

    private void open() throws SQLException {
        db1 = MzkDb.create(getParamValue(PARAM_FILE1));
        db2 = MzkDb.create(getParamValue(PARAM_FILE2));
    }

    private void compareUwagi() throws SQLException {
        reportStatus("" + db1.uwagiDao.queryForAll().size() + " " + db2.uwagiDao.queryForAll().size());
        reportStatus("" + db1.odjazdUwagaDao.queryForAll().size() + " " + db2.odjazdUwagaDao.queryForAll().size());
    }

    private void compareDatabases() throws SQLException {

        List<Kurs> kursy1 = db1.kursyDao.queryForAll();
        List<Kurs> kursy2 = db2.kursyDao.queryForAll();

        for (Kurs kurs : kursy1) {
            Optional<Kurs> optional = kursy2.stream()
                    .filter(k -> k.kierunek.equals(kurs.kierunek) && k.linia.equals(kurs.linia))
                    .findFirst();
            if (optional.isPresent()) {
                reportStatus("Porownywanie [" + kurs.linia + " - " + kurs.kierunek + "]");
                compareKursy(kurs, optional.get());
            } else {
                reportStatus("Brak kursu [" + kurs.linia + " - " + kurs.kierunek + "] w pliku " + db2.fileName);
            }
        }
    }

    private void compareKursy(Kurs kurs1, Kurs kurs2) throws SQLException {

        List<Rozklad> rozklady1 = getRozklady(db1, kurs1.id);
        List<Rozklad> rozklady2 = getRozklady(db2, kurs2.id);

        for (Rozklad rozklad : rozklady1) {
            Optional<Rozklad> optional = rozklady2.stream()
                    .filter(r -> rozklad.numer == r.numer && rozklad.przystanek.nazwa.equals(r.przystanek.nazwa))
                    .findFirst();
            if (optional.isPresent()) {
                if (compareRozklad(rozklad, optional.get())) {
                    reportStatus("Różnica");
                    break;
                }
            } else {
                reportStatus("Brak rozkladu [" + rozklad.numer + " - " + rozklad.przystanek + "] w pliku " + db2.fileName);
                break;
            }
        }
    }

    private boolean compareRozklad(Rozklad rozklad1, Rozklad rozklad2) throws SQLException {
        List<Odjazd> odjazdy1 = db1.odjazdyDao.queryForEq(Odjazd.COLUMN_ROZKLAD_ID, rozklad1.id);
        List<Odjazd> odjazdy2 = db2.odjazdyDao.queryForEq(Odjazd.COLUMN_ROZKLAD_ID, rozklad2.id);

//        if (odjazdy1.size() != odjazdy2.size()) {
//            reportStatus("[" + rozklad1.przystanek.nazwa + "] odjazdow w starym: " + odjazdy1.size() + ", w nowym: " + odjazdy2.size());
//        }

        for (Odjazd odjazd : odjazdy1) {
            Optional<Odjazd> optional = odjazdy2.stream()
                    .filter(o -> odjazd.dzien == o.dzien && odjazd.godzina == o.godzina && odjazd.minuta == o.minuta)
                    .findFirst();
//            if (optional.isPresent()) {
//
//            } else {
//                reportStatus("Brak odjazdu " + odjazd.dzien + " " + odjazd.godzina + ":" + odjazd.minuta + " na przystanku " + rozklad1.przystanek.nazwa);
//            }
            if (!optional.isPresent()) {
                return true;
            }
        }

        return false;
    }

    private List<Rozklad> getRozklady(MzkDb db, int kursId) throws SQLException {
        List<Rozklad> rozklady = db.rozkladyDao.queryBuilder()
                .orderBy(Rozklad.COLUMN_DODATKOWY, true)
                .orderBy(Rozklad.COLUMN_NUMER, true)
                .where().eq(Rozklad.COLUMN_KURS_ID, kursId)
                .query();

        for (Rozklad rozklad : rozklady) {
            db.przystankiDao.refresh(rozklad.przystanek);
        }

        return rozklady;
    }

    private void close() {
        try {
            db1.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            db2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
