package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileSplitterPlugin extends Plugin {

    public static final String PARAM_FILE = "file";
    public static final String PARAM_CHUNK_SIZE = "chunk size";

    public FileSplitterPlugin() {
        super(PARAM_FILE, PARAM_CHUNK_SIZE);
    }

    @Override
    public String getName() {
        return "File Splitter";
    }

    @Override
    public void execute() {
        super.execute();

        try {
            splitFile();
        } catch (IOException e) {
            e.printStackTrace();
            reportError(e);
        }
    }

    private void splitFile() throws IOException {

        String fileName = getParamValue(PARAM_FILE);
        int chunkSize = getParamIntValue(PARAM_CHUNK_SIZE);

        FileInputStream inputStream = new FileInputStream(fileName);
        int part = 1;
        FileOutputStream outputStream = new FileOutputStream(fileName + part);

        int count;
        int written = 0;
        byte[] buffer = new byte[1024];

        while ((count = inputStream.read(buffer)) > 0) {

            if (written + count > chunkSize) {
                written = 0;
                part++;
                outputStream.flush();
                outputStream.close();
                outputStream = new FileOutputStream(fileName + part);
            }

            outputStream.write(buffer, 0, count);
            written += count;
        }

        outputStream.flush();
        outputStream.close();
        inputStream.close();
    }
}
