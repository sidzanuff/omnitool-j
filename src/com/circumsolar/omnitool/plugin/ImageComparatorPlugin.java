package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;
import com.circumsolar.util.ImageUtils;

import javax.imageio.ImageIO;
import javax.management.OperationsException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageComparatorPlugin extends Plugin {

    public static final String PARAM_IMAGE1 = "image 1";
    public static final String PARAM_IMAGE2 = "image 2";

    public ImageComparatorPlugin() {
        super(
                PARAM_IMAGE1,
                PARAM_IMAGE2
        );
    }

    @Override
    public String getName() {
        return "Image comparator";
    }

    @Override
    public void execute() {
        super.execute();

        try {

            int diff = compare();
            reportStatus("Difference: " + diff + "%" );

        } catch (IOException | OperationsException e) {
            e.printStackTrace();
            reportError(e);
        }
    }

    public int compare() throws IOException, OperationsException {

        BufferedImage img1 = ImageIO.read(new File(getParamValue(PARAM_IMAGE1)));
        BufferedImage img2 = ImageIO.read(new File(getParamValue(PARAM_IMAGE2)));

        return ImageUtils.compareImages(img1, img2);
    }
}
