package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;
import com.circumsolar.util.HsuHou;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.Buffer;

public class HsuHouPlugin extends Plugin {

    public static final String PARAM_HOST_IMAGE_PATH = "host image";
    public static final String PARAM_SECRET_IMAGE_PATH = "secret image";
    public static final String PARAM_PRIVATE_KEY = "private key";
    public static final String PARAM_OWNERSHIP_SHARE_PATH = "ownership share";
    public static final String PARAM_MASTER_SHARE_PATH = "master share";
    public static final String PARAM_MODE = "mode";

    public HsuHouPlugin() {
        super(
                PARAM_HOST_IMAGE_PATH,
                PARAM_SECRET_IMAGE_PATH,
                PARAM_PRIVATE_KEY,
                PARAM_OWNERSHIP_SHARE_PATH,
                PARAM_MASTER_SHARE_PATH,
                PARAM_MODE
        );
    }

    @Override
    public String getName() {
        return "Hsu-Hou Algorithm";
    }

    @Override
    public void execute() {
        super.execute();

        String mode = getParamValue(PARAM_MODE);

        try {
            if (mode.startsWith("o")) {
                createOwnershipShare();
            } else if (mode.startsWith("m")){
                createMasterShare();
            } else {
                revealSecretImage();
            }
        } catch (IOException e) {
            e.printStackTrace();
            reportError(e);
        }
    }

    private void createOwnershipShare() throws IOException {

        BufferedImage hostImage = ImageIO.read(new File(getParamValue(PARAM_HOST_IMAGE_PATH)));
        BufferedImage secretImage = ImageIO.read(new File(getParamValue(PARAM_SECRET_IMAGE_PATH)));
        long privateKey = getParamLongValue(PARAM_PRIVATE_KEY);

        HsuHou hsuHou = new HsuHou(hostImage, privateKey);
        BufferedImage image = hsuHou.createOwnershipShare(secretImage);

        ImageIO.write(image, "png", new File(getParamValue(PARAM_OWNERSHIP_SHARE_PATH)));
    }

    private void createMasterShare() throws IOException {

        BufferedImage hostImage = ImageIO.read(new File(getParamValue(PARAM_HOST_IMAGE_PATH)));
        BufferedImage ownershipShare = ImageIO.read((new File(getParamValue(PARAM_OWNERSHIP_SHARE_PATH))));
        long privateKey = getParamLongValue(PARAM_PRIVATE_KEY);

        HsuHou hsuHou = new HsuHou(hostImage, privateKey);
        BufferedImage image = hsuHou.createMasterShare(ownershipShare);

        ImageIO.write(image, "png", new File(getParamValue(PARAM_MASTER_SHARE_PATH)));
    }

    private void revealSecretImage() throws IOException {

        BufferedImage hostImage = ImageIO.read(new File(getParamValue(PARAM_HOST_IMAGE_PATH)));
        BufferedImage ownershipShare = ImageIO.read((new File(getParamValue(PARAM_OWNERSHIP_SHARE_PATH))));
        long privateKey = getParamLongValue(PARAM_PRIVATE_KEY);

        HsuHou hsuHou = new HsuHou(hostImage, privateKey);
        BufferedImage image = hsuHou.revealSecretImage(ownershipShare);

        ImageIO.write(image, "png", new File(getParamValue(PARAM_SECRET_IMAGE_PATH)));
    }
}
