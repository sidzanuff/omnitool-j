package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;

import java.io.IOException;

@SuppressWarnings("UnusedDeclaration")
public class TestPlugin extends Plugin {

    public TestPlugin() {
        super("test");
    }

    @Override
    public String getName() {
        return "Test plugin";
    }

    @Override
    public void execute() {
        super.execute();

        reportStatus("warming up...");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int count = 4;
        for (int i = 0; i < count; i++) {
            reportProgress(i * 100 / count);
            reportStatus(String.valueOf(i));

            if (i == 5) {
                reportError(new IOException("some io exception"));
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

//        TextResultDialog dialog = new TextResultDialog();
//        dialog.setResult(getParamValue("test"));
//        dialog.setVisible(true);
    }

    @Override
    public void sendInput(String input) {
        reportStatus("echo: " + input);
    }
}
