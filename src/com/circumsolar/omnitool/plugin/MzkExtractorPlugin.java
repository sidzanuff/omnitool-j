package com.circumsolar.omnitool.plugin;

import com.circumsolar.mzkbb.MzkExtractor;
import com.circumsolar.omnitool.framework.Plugin;
import com.circumsolar.util.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.sql.SQLException;

public class MzkExtractorPlugin extends Plugin {

    public static final String PARAM_CACHE_PATH = "cache path";
    public static final String PARAM_DB_PATH = "db path";

    public MzkExtractorPlugin() {
        super(PARAM_CACHE_PATH, PARAM_DB_PATH);
    }

    @Override
    public String getName() {
        return "MZK BB Scrapper";
    }

    @Override
    public void execute() {
        super.execute();

        try {
            new MzkExtractor(getParamValue(PARAM_CACHE_PATH), getParamValue(PARAM_DB_PATH), logger).extract();
        } catch (Exception e) {
            e.printStackTrace();
            reportError(e);
        }
    }

    private Logger logger = new Logger() {

        @Override
        public void d(String message) {
            //reportStatus(message);
        }

        @Override
        public void i(String message) {
            reportStatus(message);
        }

        @Override
        public void e(Exception e) {
            reportError(e);
        }
    };
}
