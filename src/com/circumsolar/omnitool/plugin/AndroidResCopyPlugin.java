package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class AndroidResCopyPlugin extends Plugin {

    public static final String PARAM_ZIP_FILE = "zip file";
    public static final String PARAM_RENAME_TO = "rename to";
    public static final String PARAM_PROJECT_FOLDER = "project folder";
    public static final String PARAM_IGNORE = "ignore";

    public AndroidResCopyPlugin() {
        super(
                PARAM_ZIP_FILE,
                PARAM_RENAME_TO,
                PARAM_PROJECT_FOLDER,
                PARAM_IGNORE
        );
    }

    @Override
    public String getName() {
        return "Andriod Resource Copier";
    }

    @Override
    public void execute() {
        super.execute();

        try {
            unzip();
        } catch (IOException e) {
            e.printStackTrace();
            reportError(e);
        }
    }

    private void unzip() throws IOException {
        String zipFile = getParamValue(PARAM_ZIP_FILE);

        try (ZipInputStream stream = new ZipInputStream(new FileInputStream(zipFile))) {

            ZipEntry entry;
            while ((entry = stream.getNextEntry()) != null) {

                if (entry.isDirectory()) {
                    continue;
                }

                int startIndex = entry.getName().lastIndexOf('/') + 1;

                String entryFileName = entry.getName().substring(startIndex, entry.getName().length());

                if (paramValuesContains(PARAM_IGNORE, entryFileName, false)) {
                    continue;
                }

                File outFile;

                if (hasParam(PARAM_RENAME_TO)) {
                    outFile = new File(
                            getParamValue(PARAM_PROJECT_FOLDER),
                            entry.getName().substring(0, startIndex) + getParamValue(PARAM_RENAME_TO));
                } else {
                    outFile = new File(getParamValue(PARAM_PROJECT_FOLDER, entry.getName()));
                }

                reportStatus("Extracting " + entryFileName + " to " + outFile.getAbsolutePath());

                File dir = outFile.getParentFile();
                if (!dir.exists()) {
                    if (!dir.mkdirs()) {
                        throw new IOException("Could not create directory " + dir.getAbsolutePath());
                    }
                }

                extractEntry(stream, outFile);
            }
        }
    }

    private void extractEntry(ZipInputStream inputStream, File outFile) throws IOException {

        try(OutputStream outputStream = new FileOutputStream(outFile)) {

            int count;
            byte[] buffer = new byte[4096];

            while((count = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, count);
            }
        }
    }
}
