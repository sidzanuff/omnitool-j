package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImageBlenderPlugin extends Plugin {

    public static String PARAM_SOURCE_IMAGE_PATH = "source image";
    public static String PARAM_RECTANGLE = "rectangle";
    public static String PARAM_IMAGES_PATHS = "images paths";

    public ImageBlenderPlugin() {
        super(PARAM_SOURCE_IMAGE_PATH, PARAM_RECTANGLE, PARAM_IMAGES_PATHS);
    }

    @Override
    public String getName() {
        return "Image Blender";
    }

    @Override
    public void execute() {
        super.execute();

        int[] rectangle = getParamIntValues(PARAM_RECTANGLE);

        BufferedImage sourceImage;
        try {
            sourceImage = ImageIO.read(new File(getParamValue(PARAM_SOURCE_IMAGE_PATH)));
        } catch (IOException e) {
            reportError(e);
            return;
        }

        Graphics sourceGraphics = sourceImage.getGraphics();

        List<File> imageFiles = new ArrayList<>();

        for (String fileName : getParamValues(PARAM_IMAGES_PATHS))
        {
            File file = new File(fileName);

            if (file.isFile()) {
                imageFiles.add(file);
            } else if (file.isDirectory()) {
                imageFiles.addAll(FileUtils.listFiles(file, null, true));
            }
        }

        for(File imageFile : imageFiles) {

            reportStatus(imageFile.getAbsolutePath());
            reportProgress(imageFiles.indexOf(imageFile) * 100 / (int)imageFile.length());

            BufferedImage image;
            try {
                image = ImageIO.read(imageFile);
            } catch (IOException e) {
                reportError(e);
                continue;
            }

//            Graphics graphics = image.createGraphics();
//            graphics.drawImage(
//                    sourceImage,
//                    rectangle[0], rectangle[1], rectangle[2], rectangle[3],
//                    rectangle[0], rectangle[1], rectangle[2], rectangle[3],
//                    null);

            sourceGraphics.setColor(Color.BLACK);
            sourceGraphics.fillRect(0, 0, image.getWidth(), image.getHeight());
            sourceGraphics.drawImage(image, 0, 0, null);

            String ext = FilenameUtils.getExtension(imageFile.getName());

            File newFile = new File(imageFile.getAbsolutePath() + ".new." + ext);

            try {
                ImageIO.write(sourceImage, ext, newFile);
            } catch (IOException e) {
                reportError(e);
                continue;
            }

            imageFile.delete();
            newFile.renameTo(newFile);
        }
    }
}
