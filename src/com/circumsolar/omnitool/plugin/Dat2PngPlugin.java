package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Dat2PngPlugin extends Plugin {

    public static final String PARAM_FILES = "files";
    public static final String PARAM_WIDTH = "width";
    public static final String PARAM_HEIGHT = "height";

    public Dat2PngPlugin() {
        super(PARAM_FILES, PARAM_WIDTH, PARAM_HEIGHT);
    }

    @Override
    public String getName() {
        return "DAT2PNG";
    }

    @Override
    public void execute() {
        super.execute();

        for (String fileName : getParamValues(PARAM_FILES)) {
            try {
                convertFile(fileName);
            } catch (IOException e) {
                e.printStackTrace();
                reportError(e);
            }
        }
    }

    private void convertFile(String fileName) throws IOException {
        int width = getParamIntValue(PARAM_WIDTH);
        int height = getParamIntValue(PARAM_HEIGHT);

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        FileInputStream stream = new FileInputStream(fileName);

        byte[] buffer = new byte[4];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                stream.read(buffer);
                Color color = new Color(buffer[1] & 0xff, buffer[2] & 0xff, buffer[3] & 0xff, buffer[0] & 0xff);
                image.setRGB(x, y, color.getRGB());
            }
        }

        ImageIO.write(image, "png", new File(fileName + ".png"));
    }
}
