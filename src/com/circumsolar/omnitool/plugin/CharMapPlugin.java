package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@SuppressWarnings("UnusedDeclaration")
public class CharMapPlugin extends Plugin {

    public static final String PARAM_PATHS = "paths";
    public static final String PARAM_EXTENSIONS = "extensions";
    public static final String PARAM_BASE_CHARS = "base characters";
    public static final String PARAM_CHARSET = "charset";

    private List<Character> characters;

    public CharMapPlugin() {
        super(
                PARAM_PATHS,
                PARAM_EXTENSIONS,
                PARAM_BASE_CHARS,
                PARAM_CHARSET);
    }

    @Override
    public String getName() {
        return "Character map generator";
    }

    @Override
    public void execute() {
        super.execute();

        String[] paths = getParamValues(PARAM_PATHS);
        characters = new ArrayList<>();

        String baseChars = getParamValue(PARAM_BASE_CHARS);
        for (int i = 0; i < baseChars.length(); i++) {
            char c = baseChars.charAt(i);
            if (!Character.isWhitespace(c) && !characters.contains(c)) {
                characters.add(c);
            }
        }

        for (String path : paths) {

            if (isCancelled()) {
                break;
            }

            File file = new File(path);
            if (file.isDirectory()) {
                Iterator files = FileUtils.iterateFiles(file, getParamNullableValues(PARAM_EXTENSIONS), true);
                while (files.hasNext()) {

                    if (isCancelled()) {
                        break;
                    }

                    file = (File) files.next();
                    processFile(file);
                }
            } else {
                processFile(file);
            }
        }

        StringBuilder sb = new StringBuilder();
        for (Character c : characters) {
            sb.append(c);
        }

        reportStatus("\n" + sb.toString());
    }

    private void processFile(File file) {
        try (InputStream stream = new FileInputStream(file);
             InputStreamReader reader = new InputStreamReader(stream, getParamValue(PARAM_CHARSET, "UTF-8"))) {

            char[] buffer = new char[4096];
            int count;

            while ((count = reader.read(buffer, 0, buffer.length)) > 0) {

                if (isCancelled()) {
                    return;
                }

                for (int i = 0; i < count; i++) {
                    char c = buffer[i];

                    if (Character.isWhitespace(c)) {
                        continue;
                    }

                    if (!characters.contains(c)) {
                        characters.add(c);
                    }
                }
            }

        } catch (IOException e) {
            reportError(e);
        }
    }
}
