package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;
import com.circumsolar.omnitool.gui.TextResultDialog;
import com.circumsolar.subtitles.Subtitles;

public class OffsetCalculator extends Plugin {

    public static final String PARAM_SRC_TIMESTAMP = "source timestamp";
    public static final String PARAM_DST_TIMESTAMP = "destination timestamp";

    public OffsetCalculator() {
        super(PARAM_SRC_TIMESTAMP, PARAM_DST_TIMESTAMP);
    }

    @Override
    public String getName() {
        return "Offset calculator";
    }

    @Override
    public void execute() {
        super.execute();

        long srcTime = Subtitles.parseTime(getParamValue(PARAM_SRC_TIMESTAMP));
        long dstTime = Subtitles.parseTime(getParamValue(PARAM_DST_TIMESTAMP));
        long offset = dstTime - srcTime;

        TextResultDialog dialog = new TextResultDialog();
        dialog.setResult(String.valueOf(offset));
        dialog.setVisible(true);
    }
}
