package com.circumsolar.omnitool.plugin;

import com.circumsolar.omnitool.framework.Plugin;
import com.circumsolar.util.IO;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@SuppressWarnings("UnusedDeclaration")
public class LitteraAssistantPlugin extends Plugin {

    public static final String PARAM_FONT_FOLDER = "font folder";
    public static final String PARAM_PROJECT_FOLDER = "project folder";

    public LitteraAssistantPlugin() {
        super(PARAM_FONT_FOLDER, PARAM_PROJECT_FOLDER);
    }

    @Override
    public String getName() {
        return "Littera Assistant";
    }

    @Override
    public void execute() {
        super.execute();

        deleteBin();

        File contentFolder = new File(getParamValue(PARAM_PROJECT_FOLDER) + "\\Assets\\fonts");
        File[] folders = new File(getParamValue(PARAM_FONT_FOLDER)).listFiles();

        byLocale(folders, contentFolder);
    }

    private void byLocale(File[] localeFolders, File contentFolder) {
        if (localeFolders != null) {
            for (File localeFolder : localeFolders) {

                String locale = localeFolder.getName();
                File[] fontFolders = localeFolder.listFiles();

                if (fontFolders != null) {
                    for (File fontFolder : fontFolders) {

                        String postFix;

                        try {
                            unZipIt(new File(fontFolder, "font.zip"), new File(contentFolder, locale), "");
                        } catch (Exception e) {
                            e.printStackTrace();
                            reportError(e);
                        }
                    }
                }
            }
        }
    }

    private void bySize(File[] sizeFolders, File contentFolder) {
        if (sizeFolders != null) {
            for (File sizeFolder : sizeFolders) {

                File[] localeFolders = sizeFolder.listFiles();
                if (localeFolders != null) {
                    for (File localeFolder : localeFolders) {

                        String locale = localeFolder.getName();
                        File[] fontFolders = localeFolder.listFiles();

                        if (fontFolders != null) {
                            for (File fontFolder : fontFolders) {

                                String postFix;

                                if (sizeFolder.getName().toLowerCase().equals("small")) {
                                    postFix = "small";
                                } else {
                                    postFix = "";
                                }

                                try {
                                    unZipIt(new File(fontFolder, "font.zip"), new File(contentFolder, locale), postFix);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    reportError(e);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void unZipIt(File zipFile, File outputFolder, String postFix) throws IOException, NoSuchAlgorithmException {

        byte[] buffer = new byte[1024];

        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile))) {
            ZipEntry ze = zis.getNextEntry();
            while (ze != null) {

                String fileName = ze.getName();
                File dstFile = new File(outputFolder, zipFile.getParentFile().getName() + postFix +
                        fileName.substring(fileName.indexOf('.')));

                File tmpFile = File.createTempFile("omnitool", ".tmp");

                FileOutputStream fos = new FileOutputStream(tmpFile);

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                fos.close();
                ze = zis.getNextEntry();

                if (!dstFile.exists()) {
                    moveFile(tmpFile, dstFile);
                } else {

                    byte[] tmpDigest = IO.getDigest(tmpFile);
                    byte[] dstDigest = IO.getDigest(dstFile);

                    if (!Arrays.equals(tmpDigest, dstDigest)) {
                        moveFile(tmpFile, dstFile);
                    }
                }

                if (tmpFile.exists()) {
                    if (!tmpFile.delete()) {
                        throw new IOException("Could not delete file " + tmpFile.getAbsolutePath());
                    }
                }
            }

            zis.closeEntry();
        }
    }

    private void moveFile(File src, File dst) throws IOException {

        if (dst.exists()) {
            if (!dst.delete()) {
                throw new IOException("Failed to update file " + dst.getAbsolutePath());
            }
        }

        if (src.renameTo(dst)) {
            reportStatus("updated " + dst.getAbsolutePath());
        } else {
            throw new IOException("Failed to update file " + dst.getAbsolutePath());
        }
    }

    private void deleteBin() {

        File binFolder = new File(getParamValue(PARAM_PROJECT_FOLDER), "bin");
        if (!binFolder.exists()) {
            return;
        }

        File[] binFiles = binFolder.listFiles();
        if (binFiles == null) {
            return;
        }

        for (File file : binFiles) {
            try {
                reportStatus("deleting " + file.getAbsolutePath());
                FileUtils.forceDelete(file);
            } catch (IOException e) {
                e.printStackTrace();
                reportError(e);
            }
        }
    }
}
