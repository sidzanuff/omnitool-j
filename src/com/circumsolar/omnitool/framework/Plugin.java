package com.circumsolar.omnitool.framework;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class Plugin {

    private static final String[] TRUE_VALUES = new String[]{"true", "t", "yes", "y", "1"};

    private List<Param> params;
    private boolean cancelled;
    private List<PluginListener> listeners = new ArrayList<>();

    public Plugin(String... paramNames) {
        params = new ArrayList<>();
        for (String name : paramNames) {
            Param param = new Param();
            param.name = name;
            params.add(param);
        }
    }

    public abstract String getName();

    public void addListener(PluginListener listener) {
        listeners.add(listener);
    }

    public void removeListener(PluginListener listener) {
        listeners.remove(listener);
    }

    public void execute() {
        cancelled = false;
    }

    public List<Param> getParams() {
        return params;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void cancel() {
        cancelled = true;
    }

    public void sendInput(String input) {

    }

    public void reportError(Exception e) {
        for (PluginListener listener : listeners) {
            listener.reportError(e);
        }
    }

    public void reportStatus(String message) {
        System.out.println(new Date().toString() + " " + getName() + ": " + message);
        for (PluginListener listener : listeners) {
            listener.reportStatus(message);
        }
    }

    public void reportProgress(int progress) {
        for (PluginListener listener : listeners) {
            listener.reportProgress(progress);
        }
    }

    public boolean hasParam(String name) {
        Param param = getParam(name);
        return param != null && StringUtils.isNotBlank(param.value);
    }

    public String getParamValue(String name) {
        if (hasParam(name)) {
            return getParam(name).value;
        }
        return "";
    }

    public String getParamValue(String name, String defaultValue) {
        if (hasParam(name)) {
            return getParamValue(name);
        } else {
            return defaultValue;
        }
    }

    public String[] getParamValues(String name) {
        return getParamValue(name).split(",");
    }

    public int[] getParamIntValues(String name) {

        String[] values = getParamValues(name);

        int[] intValues = new int[values.length];

        for (int i = 0; i < values.length; i++) {
            intValues[i] = Integer.parseInt(values[i].trim());
        }

        return intValues;
    }

    public String[] getParamNullableValues(String name) {
        return hasParam(name) ? getParamValues(name) : null;
    }

    public int getParamIntValue(String name) {
        try {
            return Integer.parseInt(getParamValue(name));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public long getParamLongValue(String name) {
        try {
            return Long.parseLong(getParamValue(name));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public float getParamFloatValue(String name) {
        try {
            return Float.parseFloat(getParamValue(name));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public boolean getParamBoolValue(String name) {
        String value = getParamValue(name).toLowerCase();
        for (String trueValue : TRUE_VALUES) {
            if (value.equals(trueValue)) {
                return true;
            }
        }
        return false;
    }

    public boolean paramValuesContains(String name, String value, boolean caseSensitive) {
        String[] values = getParamValues(name);
        for (String v : values) {
            if (caseSensitive) {
                if (value.equals(v)) {
                    return true;
                }
            } else {
                if (value.toLowerCase().equals(v.toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }

    private Param getParam(String name) {
        for (Param param : params) {
            if (param.name.equals(name)) {
                return param;
            }
        }
        return null;
    }
}
