package com.circumsolar.omnitool.framework;

import java.util.ArrayList;
import java.util.List;

public class Config {
    public List<PluginConfig> pluginConfigs;
    public String selectedPlugin;

    public Config() {
        pluginConfigs = new ArrayList<>();
    }
}
