package com.circumsolar.omnitool.framework;

import java.util.List;

public class PluginConfig {
    public String name;
    public List<Param> params;
}
