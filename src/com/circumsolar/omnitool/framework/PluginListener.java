package com.circumsolar.omnitool.framework;

public interface PluginListener {

    void reportProgress(int progress);
    void reportStatus(String message);
    void reportError(Exception error);
}
