package com.circumsolar.omnitool.framework;

public class EchoPlugin extends Plugin {

    @Override
    public String getName() {
        return "Echo";
    }

    @Override
    public void sendInput(String input) {
        reportStatus(input);
    }
}
