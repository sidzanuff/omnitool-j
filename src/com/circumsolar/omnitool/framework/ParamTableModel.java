package com.circumsolar.omnitool.framework;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.List;

public class ParamTableModel implements TableModel {

    private static final String[] COLUMN_NAMES = new String[]{"Name", "Value"};

    private List<TableModelListener> listeners = new ArrayList<>();
    private List<Param> params = new ArrayList<>();

    @Override
    public int getRowCount() {
        return params.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == 1;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Param param = params.get(rowIndex);
        if (columnIndex == 0) {
            return param.name;
        }
        return param.value == null ? "" : param.value;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        params.get(rowIndex).value = (String)aValue;
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }

    public void setParams(List<Param> params) {
        this.params = params;
        for (TableModelListener l : listeners) {
            l.tableChanged(new TableModelEvent(this));
        }
    }
}
