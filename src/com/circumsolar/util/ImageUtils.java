package com.circumsolar.util;

import javax.management.OperationsException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;

public class ImageUtils {

    public static final int SCALE_MODE_FIT = 0;
    public static final int SCALE_MODE_CROP = 1;

    public static void scaleImage(Graphics2D graphics, BufferedImage image, Dimension size, int mode) {
        scaleImage(graphics, image, size, mode, 0, 0);
    }

    public static void scaleImage(Graphics2D graphics, BufferedImage image, Dimension size, int mode, int x, int y) {

        Dimension srcSize;
        Point srcOffset;
        Dimension destSize;
        Point destOffset;

        if (mode == SCALE_MODE_FIT) {
            srcSize = new Dimension(image.getWidth(), image.getHeight());
            srcOffset = new Point();
            destSize = ImageUtils.rescale(srcSize, size);
            destOffset = new Point(
                    (int) (x + (size.getWidth() - destSize.getWidth()) / 2),
                    (int) (y + (size.getHeight() - destSize.getHeight()) / 2));
        } else if (mode == SCALE_MODE_CROP) {
            srcSize = ImageUtils.rescale(size, new Dimension(image.getWidth(), image.getHeight()));
            srcOffset = new Point((int) ((image.getWidth() - srcSize.getWidth()) / 2), (int) ((image.getHeight() - srcSize.getHeight()) / 2));
            destSize = size;
            destOffset = new Point(x, y);
        } else {
            destSize = new Dimension(image.getWidth(), image.getHeight());
            srcOffset = new Point();
            srcSize = new Dimension(image.getWidth(), image.getHeight());
            destOffset = new Point(
                    (int) (x + (size.getWidth() - destSize.getWidth()) / 2),
                    (int) (y + (size.getHeight() - destSize.getHeight()) / 2));
        }

        if (destSize.width < srcSize.width && destSize.height < srcSize.height) {
            BufferedImage src = new BufferedImage(srcSize.width, srcSize.height, BufferedImage.TYPE_INT_ARGB);
            Graphics srcGraphics = src.getGraphics();
            srcGraphics.drawImage(
                    image,
                    0, 0, srcSize.width, srcSize.height,
                    srcOffset.x, srcOffset.y, srcSize.width, srcSize.height,
                    null);

            graphics.drawImage(
                    ImageUtils.getScaledInstance(
                            src,
                            (int) destSize.getWidth(),
                            (int) destSize.getHeight(),
                            RenderingHints.VALUE_INTERPOLATION_BICUBIC,
                            true),
                    destOffset.x,
                    destOffset.y,
                    (int) destSize.getWidth(),
                    (int) destSize.getHeight(),
                    null);
        } else {
            graphics.drawImage(
                    image,
                    destOffset.x,
                    destOffset.y,
                    destOffset.x + (int) destSize.getWidth(),
                    destOffset.y + (int) destSize.getHeight(),
                    srcOffset.x,
                    srcOffset.y,
                    srcOffset.x + (int) srcSize.getWidth(),
                    srcOffset.y + (int) srcSize.getHeight(),
                    null);
        }
    }

    public static Dimension rescale(Dimension source, Dimension dest) {

        double scale;
        double widthScale;
        double heightScale;

        widthScale = dest.getWidth() / source.getWidth();
        heightScale = dest.getHeight() / source.getHeight();

        if (heightScale < widthScale)
            scale = heightScale;
        else
            scale = widthScale;

        return new Dimension(
                (int) (source.getWidth() * scale),
                (int) (source.getHeight() * scale));
    }

    public static BufferedImage getScaledInstance(BufferedImage img,
                                                  int targetWidth,
                                                  int targetHeight,
                                                  Object hint,
                                                  boolean higherQuality) {
        int type = (img.getTransparency() == Transparency.OPAQUE) ?
                BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage ret = (BufferedImage) img;
        int w, h;
        if (higherQuality) {
            // Use multi-step technique: start with original size, then
            // scale down in multiple passes with drawImage()
            // until the target size is reached
            w = img.getWidth();
            h = img.getHeight();
        } else {
            // Use one-step technique: scale directly from original
            // size to target size with a single drawImage() call
            w = targetWidth;
            h = targetHeight;
        }

        do {
            if (higherQuality && w > targetWidth) {
                w /= 2;
                if (w < targetWidth) {
                    w = targetWidth;
                }
            }

            if (higherQuality && h > targetHeight) {
                h /= 2;
                if (h < targetHeight) {
                    h = targetHeight;
                }
            }

            BufferedImage tmp = new BufferedImage(w, h, type);
            Graphics2D g2 = tmp.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
            g2.drawImage(ret, 0, 0, w, h, null);
            g2.dispose();

            ret = tmp;
        } while (w != targetWidth || h != targetHeight);

        return ret;
    }

    public static int compareImages(BufferedImage img1, BufferedImage img2) throws OperationsException {

        int width1 = img1.getWidth(null);
        int width2 = img2.getWidth(null);
        int height1 = img1.getHeight(null);
        int height2 = img2.getHeight(null);

        if ((width1 != width2) || (height1 != height2)) {
            throw new OperationsException("Images dimensions mismatch");
        }

        long diff = 0;

        for (int y = 0; y < height1; y++) {
            for (int x = 0; x < width1; x++) {

                int rgb1 = img1.getRGB(x, y);
                int rgb2 = img2.getRGB(x, y);

                int r1 = (rgb1 >> 16) & 0xff;
                int g1 = (rgb1 >> 8) & 0xff;
                int b1 = (rgb1) & 0xff;

                int r2 = (rgb2 >> 16) & 0xff;
                int g2 = (rgb2 >> 8) & 0xff;
                int b2 = (rgb2) & 0xff;

                diff += Math.abs(r1 - r2);
                diff += Math.abs(g1 - g2);
                diff += Math.abs(b1 - b2);
            }
        }

        double n = width1 * height1 * 3;
        double p = diff / n / 255.0;

        return (int) (p * 100.0);
    }
}
