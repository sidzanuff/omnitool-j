package com.circumsolar.util;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.util.Random;

public class HsuHou {

    private static final int DEFAULT_SAMPLE_SIZE = 30;

    private BufferedImage hostImage;
    private Random random;
    private int hostImageMean;
    private WritableRaster raster;

    private int[] pixelArray = new int[]{0, 0, 0, 255};

    public HsuHou(BufferedImage hostImage, long privateKey) {

        this.hostImage = hostImage;
        random = new Random(privateKey);

        calculateHostImageMean();
    }

    public BufferedImage createOwnershipShare(BufferedImage secretImage) {

        BufferedImage ownershipShareImage = new BufferedImage(
                secretImage.getWidth() * 2,
                secretImage.getHeight() * 2,
                BufferedImage.TYPE_INT_BGR);

        raster = ownershipShareImage.getRaster();

        Raster secretRaster = secretImage.getRaster();

        for (int x = 0; x < secretImage.getWidth(); ++x) {
            for (int y = 0; y < secretImage.getHeight(); ++y) {

                int sampleMean = calculateNextSampleMean();
                boolean secretPixelBlack = secretRaster.getSample(x, y, 0) == 0;
                boolean pattern;

                if (secretPixelBlack && sampleMean < hostImageMean) {
                    pattern = true;
                } else if (!secretPixelBlack && sampleMean >= hostImageMean) {
                    pattern = true;
                } else if (!secretPixelBlack && sampleMean < hostImageMean) {
                    pattern = false;
                } else {
                    pattern = false;
                }

                setPattern(x, y, pattern);
            }
        }

        return ownershipShareImage;
    }

    public BufferedImage createMasterShare(BufferedImage ownershipShareImage) {

        BufferedImage masterShare = new BufferedImage(
                ownershipShareImage.getWidth(),
                ownershipShareImage.getHeight(),
                BufferedImage.TYPE_INT_BGR);

        raster = masterShare.getRaster();

        for (int x = 0; x < masterShare.getWidth() / 2; ++x) {
            for (int y = 0; y < masterShare.getHeight() / 2; ++y) {

                int sampleMean = calculateNextSampleMean();
                boolean pattern = sampleMean < hostImageMean;

                setPattern(x, y, pattern);
            }
        }

        return masterShare;
    }

    public BufferedImage revealSecretImage(BufferedImage ownershipShareImage) {

        BufferedImage secretImage = new BufferedImage(
                ownershipShareImage.getWidth() / 2,
                ownershipShareImage.getHeight() / 2,
                BufferedImage.TYPE_INT_BGR);

        raster = secretImage.getRaster();

        Raster shareRaster = ownershipShareImage.getRaster();

        for (int x = 0; x < ownershipShareImage.getWidth() / 2; ++x) {
            for (int y = 0; y < ownershipShareImage.getHeight() / 2; ++y) {

                int sampleMean = calculateNextSampleMean();
                boolean pattern = shareRaster.getSample(x * 2, y * 2, 0) == 0;
                boolean color;

                if (pattern && sampleMean < hostImageMean) {
                    color = false;
                } else if (pattern && sampleMean >= hostImageMean) {
                    color = true;
                } else if (!pattern && sampleMean < hostImageMean) {
                    color = true;
                } else {
                    color = false;
                }

                setPixel(x, y, color);
            }
        }

        return secretImage;
    }

    private void calculateHostImageMean() {

        long sum = 0;

        for (int y = 0; y < hostImage.getHeight(); ++y) {
            for (int x = 0; x < hostImage.getWidth(); ++x) {
                sum += calculatePixelMean(x, y);
            }
        }

        long pixelCount = hostImage.getWidth() * hostImage.getHeight();
        long mean = sum / pixelCount;

        hostImageMean = (int) mean;
    }

    private int calculatePixelMean(int x, int y) {

        int pixel = hostImage.getRGB(x, y);

        int red = (pixel & 0x00ff0000) >> 16;
        int green = (pixel & 0x0000ff00) >> 8;
        int blue = pixel & 0x000000ff;

        return (red + green + blue) / 3;
    }

    private int calculateNextSampleMean() {

        long sum = 0;

        for (int i = 0; i < DEFAULT_SAMPLE_SIZE; ++i) {

            int x = random.nextInt(hostImage.getWidth());
            int y = random.nextInt(hostImage.getHeight());

            sum += calculatePixelMean(x, y);
        }

        return (int) (sum / DEFAULT_SAMPLE_SIZE);
    }

    private void setPattern(int x, int y, boolean pattern) {

        setPixel(x * 2, y * 2, !pattern);
        setPixel(x * 2 + 1, y * 2, pattern);
        setPixel(x * 2, y * 2 + 1, pattern);
        setPixel(x * 2 + 1, y * 2 + 1, !pattern);
    }

    private void setPixel(int x, int y, boolean black) {

        pixelArray[0] = black ? 0 : 255;
        pixelArray[1] = black ? 0 : 255;
        pixelArray[2] = black ? 0 : 255;

        raster.setPixel(x, y, pixelArray);
    }
}
