package com.circumsolar.util;

public interface Logger {

    public void d(String message);

    public void i(String message);

    public void e(Exception e);
}
