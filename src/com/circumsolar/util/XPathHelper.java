package com.circumsolar.util;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class XPathHelper {

    private final HtmlCleaner htmlCleaner;
    private final XPath xPath;
    private final File cachePath;
    private final DomSerializer domSerializer;
    private final String encoding;
    private final Logger logger;

    public XPathHelper(String cachePath, Logger logger, String encoding) {

        htmlCleaner = new HtmlCleaner();
        xPath = XPathFactory.newInstance().newXPath();
        domSerializer = new DomSerializer(new CleanerProperties());

        this.cachePath = new File(cachePath);
        this.logger = logger;
        this.encoding = encoding;

        if (!this.cachePath.exists()) {
            this.cachePath.mkdirs();
        }
    }

    public NodeList l(String expression, String url) throws IOException, ParserConfigurationException,
            XPathExpressionException {
        TagNode tagNode = htmlCleaner.clean(getPage(url), encoding);
        Document doc = domSerializer.createDOM(tagNode);
        return (NodeList) xPath.evaluate(expression, doc, XPathConstants.NODESET);
    }

    public String v(String expression, Node node) throws XPathExpressionException, UnsupportedEncodingException {
        return fixUTF8(xPath.evaluate(expression, node, XPathConstants.STRING));
    }

    public String v(String expression, String url) throws XPathExpressionException, IOException, ParserConfigurationException {
        TagNode tagNode = htmlCleaner.clean(getPage(url), encoding);
        Document doc = domSerializer.createDOM(tagNode);
        return fixUTF8(xPath.evaluate(expression, doc, XPathConstants.STRING));
    }

    public static String fixUTF8(Object s) {
        return ((String) s)
                .trim()
                .replace("&oacute;", "ó")
                .replace("&Oacute;", "Ó")
                .replace("\u00A0", "")
                .replace("&amp;", "&")
                .replace("&quot;", "\"");
    }

    public void c(String url) throws IOException {
        try {
            getPage(url);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException("Bład podczas pobierania strony " + url);
        }
    }

    public File getPage(String url) throws IOException {
        String fileName = URLEncoder.encode(url, encoding);
        File file = new File(cachePath, fileName);

        if (!file.exists()) {
            logger.d("Downloading page " + url);
            URL website = new URL(url);
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            FileOutputStream fos = new FileOutputStream(file);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
        } else {
            logger.d("Loading cached page " + url);
        }

        return file;
    }
}
