package com.circumsolar.subtitles;

public class Subtitle {
    public long startTime;
    public long endTime;
    public String subtitle;

    public long getDuration() {
        return endTime - startTime;
    }

    public void setDuration(long duration) {
        endTime = startTime + duration;
    }
}