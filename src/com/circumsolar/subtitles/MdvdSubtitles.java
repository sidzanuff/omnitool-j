package com.circumsolar.subtitles;

import org.apache.commons.lang3.StringUtils;

public class MdvdSubtitles extends Subtitles {

    @Override
    protected Subtitle parseLine(String line) {
        int idx1 = line.indexOf('}');
        int idx2 = line.indexOf('}', idx1 + 1);

        if (idx1 == -1 || idx2 == -1) {
            return null;
        }

        String startFrameStr = line.substring(1, idx1);
        if (StringUtils.isBlank(startFrameStr)) {
            return null;
        }

        int startFrame = Integer.parseInt(startFrameStr);

        String endFrameStr = line.substring(idx1 + 2, idx2);
        int endFrame;

        if (StringUtils.isBlank(endFrameStr)) {
            endFrame = startFrame;
        } else {
            endFrame = Integer.parseInt(endFrameStr);
        }

        Subtitle subtitle = new Subtitle();
        subtitle.startTime = (long)(startFrame / getFps() * 1000);
        subtitle.endTime = (long)(endFrame / getFps() * 1000);
        subtitle.subtitle = line.substring(idx2 + 1).replace("|", "\r\n");
        return subtitle;
    }
}
