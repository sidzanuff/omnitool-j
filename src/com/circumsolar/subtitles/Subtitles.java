package com.circumsolar.subtitles;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public abstract class Subtitles {

    private List<Subtitle> subtitles = new ArrayList<>();
    private String charsetName;
    private float fps;

    public List<Subtitle> getSubtitles() {
        return subtitles;
    }

    public void setSubtitles(List<Subtitle> subtitles) {
        this.subtitles = subtitles;
    }

    public String getCharsetName() {
        return charsetName;
    }

    public void setCharsetName(String charsetName) {
        this.charsetName = charsetName;
    }

    public float getFps() {
        return fps;
    }

    public void setFps(float fps) {
        this.fps = fps;
    }

    public void read(String fileName) throws IOException {
        try (InputStream stream = new FileInputStream(fileName);
             InputStreamReader reader = new InputStreamReader(stream, charsetName);
             BufferedReader bufferedReader = new BufferedReader(reader)) {

            beforeParse(bufferedReader);

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                Subtitle subtitle = parseLine(line);
                if (subtitle != null) {
                    subtitles.add(subtitle);
                }
            }
        }

        afterParse();
    }

    public void write(String fileName) throws IOException {
        try (OutputStream stream = new FileOutputStream(fileName);
             OutputStreamWriter writer = new OutputStreamWriter(stream, charsetName)) {

            beforeWrite(writer);

            for (int i = 0; i < subtitles.size(); i++) {
                writeSubtitle(writer, i + 1, subtitles.get(i));
            }

            afterWrite(writer);
        }
    }

    protected void beforeParse(BufferedReader bufferedReader) {

    }

    protected Subtitle parseLine(String line) {
        throw new NotImplementedException();
    }

    protected void afterParse() {

    }

    protected void beforeWrite(Writer writer) {

    }

    protected void writeSubtitle(Writer writer, int index, Subtitle subtitle) throws IOException {
        throw new NotImplementedException();
    }

    protected void afterWrite(Writer writer) {

    }

    public static String formatTime(long millis) {

        long hrs = TimeUnit.MILLISECONDS.toHours(millis) % 24;
        long min = TimeUnit.MILLISECONDS.toMinutes(millis) % 60;
        long sec = TimeUnit.MILLISECONDS.toSeconds(millis) % 60;
        long mls = millis % 1000;

        return String.format("%02d:%02d:%02d.%03d", hrs, min, sec, mls);
    }

    public static long parseTime(String time) {
        int hrs = parseNumber(time, 0, 2);
        int min = parseNumber(time, 3, 2);
        int sec = parseNumber(time, 6, 2);
        int mil = parseNumber(time, 9);

        return TimeUnit.HOURS.toMillis(hrs) + TimeUnit.MINUTES.toMillis(min) + TimeUnit.SECONDS.toMillis(sec) + mil;
    }

    private static int parseNumber(String str, int startIdx) {
        return parseNumber(str, startIdx, -1);
    }

    private static int parseNumber(String str, int startIdx, int len) {
        try {
            String subStr = len > 0 ? str.substring(startIdx, startIdx + len) : str.substring(startIdx);
            return Integer.parseInt(subStr);
        } catch (NumberFormatException e) {
            return 0;
        } catch (StringIndexOutOfBoundsException e) {
            return 0;
        }
    }
}
