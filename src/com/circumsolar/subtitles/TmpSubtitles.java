package com.circumsolar.subtitles;

public class TmpSubtitles extends Subtitles {

    private static final int MIN_SPAN = 1000;
    private static final int MAX_SPAN = 5000;

    @Override
    protected Subtitle parseLine(String line) {
        if (line.equals("")) {
            return null;
        }

        String timeStr = line.substring(0, 8);
        long time = parseTime(timeStr);

        adjustLastSubtitle(time);

        String subStr = line.substring(9, line.length()).replace("|", "\r\n");

        Subtitle subtitle = new Subtitle();
        subtitle.startTime = time;
        subtitle.subtitle = subStr;

        return subtitle;
    }

    @Override
    protected void afterParse() {
        adjustLastSubtitle(0);
    }

    private void adjustLastSubtitle(long time) {
        if (getSubtitles().size() > 0) {
            Subtitle lastSub = getSubtitles().get(getSubtitles().size() - 1);
            lastSub.endTime = time;
            if (lastSub.getDuration() < MIN_SPAN) {
                lastSub.setDuration(MIN_SPAN);
            } else if (lastSub.getDuration() > MAX_SPAN) {
                lastSub.setDuration(MAX_SPAN);
            }
        }
    }
}
