package com.circumsolar.subtitles;

import java.io.IOException;
import java.io.Writer;

public class SubRipSubtitles extends Subtitles {

    @Override
    protected void writeSubtitle(Writer writer, int index, Subtitle subtitle) throws IOException {
        writer.write(index + "\r\n");
        writer.write(formatTime(subtitle.startTime));
        writer.write(" --> ");
        writer.write(formatTime(subtitle.endTime));
        writer.write("\r\n");
        writer.write(subtitle.subtitle);
        writer.write("\r\n\r\n");
    }
}
