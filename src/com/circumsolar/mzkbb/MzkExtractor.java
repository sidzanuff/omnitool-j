package com.circumsolar.mzkbb;

import com.circumsolar.util.Logger;
import com.circumsolar.util.XPathHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class MzkExtractor {

    private static final String BASE_URL = "http://mzkb-b.internetdsl.pl/";
    private static final String LINIE_URL = "linie_r.htm";
    private static final String DB_SCHEMA = "jdbc:sqlite:";

    private final XPathHelper x;
    private final Logger logger;
    private final String dbFile;

    Dao<Kurs, Integer> kursyDao;
    Dao<Odjazd, Integer> odjazdyDao;
    Dao<Przystanek, Integer> przystankiDao;
    Dao<Rozklad, Integer> rozkladyDao;
    Dao<Uwaga, Integer> uwagiDao;
    Dao<OdjazdUwaga, Integer> odjazdUwagaDao;

    Kurs kurs;
    List<Uwaga> uwagi;
    Rozklad rozklad;

    public MzkExtractor(String cachePath, String dbFile, Logger logger) {
        x = new XPathHelper(cachePath, logger, "ISO-8859-2");
        this.logger = logger;
        this.dbFile = dbFile;
    }

    public void extract() throws ParserConfigurationException, XPathExpressionException, IOException, SQLException {

        File f = new File(dbFile);
        if (f.exists()) {
            f.delete();
        }

        ConnectionSource conn = new JdbcConnectionSource(DB_SCHEMA + dbFile);

        kursyDao = DaoManager.createDao(conn, Kurs.class);
        odjazdyDao = DaoManager.createDao(conn, Odjazd.class);
        przystankiDao = DaoManager.createDao(conn, Przystanek.class);
        rozkladyDao = DaoManager.createDao(conn, Rozklad.class);
        uwagiDao = DaoManager.createDao(conn, Uwaga.class);
        odjazdUwagaDao = DaoManager.createDao(conn, OdjazdUwaga.class);

        TableUtils.createTableIfNotExists(conn, Kurs.class);
        TableUtils.createTableIfNotExists(conn, Odjazd.class);
        TableUtils.createTableIfNotExists(conn, Przystanek.class);
        TableUtils.createTableIfNotExists(conn, Rozklad.class);
        TableUtils.createTableIfNotExists(conn, Uwaga.class);
        TableUtils.createTableIfNotExists(conn, OdjazdUwaga.class);

        NodeList rows = x.l("/html/body/table/tbody/tr", BASE_URL + LINIE_URL);
        for (int i = 0; i < rows.getLength(); i++) {
            final Node node = rows.item(i);
            if (node.getChildNodes().getLength() == 11)
                TransactionManager.callInTransaction(conn, new Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        parseLinia(node);
                        return null;
                    }
                });
        }

        conn.close();
    }

    private void parseLinia(Node node) throws XPathExpressionException, IOException, ParserConfigurationException, SQLException {
        String linia = x.v("./td[2]", node);
        String kierunek;
        String kierunekUrl;

        kierunek = x.v("./td[3]", node);
        kierunekUrl = x.v("./td[3]/a/@href", node);
        kurs = new Kurs(linia, kierunek);
        kursyDao.create(kurs);
        logger.i(linia + " / " + kierunek);
        parseKierunek(BASE_URL + kierunekUrl);

        kierunek = x.v("./td[4]", node);
        kierunekUrl = x.v("./td[4]/a/@href", node);
        kurs = new Kurs(linia, kierunek);
        kursyDao.create(kurs);
        logger.i(linia + " / " + kierunek);
        parseKierunek(BASE_URL + kierunekUrl);
    }

    void parseKierunek(String url) throws ParserConfigurationException, XPathExpressionException, IOException, SQLException {
        parsujPrzystanki(false, x.l("/html/body/table[2]/tbody/tr/td[2]/table[2]/tbody/tr", url));
        parsujPrzystanki(true, x.l("/html/body/table[2]/tbody/tr/td[2]/table[4]/tbody/tr", url));
    }

    void parsujPrzystanki(boolean dodatkowe, NodeList nodeList) throws XPathExpressionException, IOException, SQLException, ParserConfigurationException {
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node n = nodeList.item(i);

            String s = x.v("./td[2]", n);
            int bracketIdx = s.indexOf(')');
            int id = Integer.parseInt(s.substring(1, bracketIdx));
            String nazwa = s.substring(bracketIdx + 1);

            Przystanek przystanek = przystankiDao.queryForId(id);
            if (przystanek != null) {
                assert przystanek.nazwa.equals(nazwa);
            }
            else {
                przystanek = new Przystanek(id, nazwa);
                przystankiDao.create(przystanek);
            }

            int numer = Integer.parseInt(x.v("./td[1]", n));
            rozklad = new Rozklad(kurs, przystanek, numer, dodatkowe);
            rozkladyDao.create(rozklad);

            String rozkladUrl = BASE_URL + x.v("./td[2]/a/@href", n).replace("_m", "");
            parsujRozklad(rozkladUrl);
        }
    }

    void parsujRozklad(String url) throws ParserConfigurationException, XPathExpressionException, IOException, SQLException {
        try {
            x.c(url);
        } catch(Exception e) {
            e.printStackTrace();
            logger.e(e);
            return;
        }

        uwagi = new ArrayList<Uwaga>();
        String s = x.v("/html/body/table[5]", url);
        String[] lines = StringUtils.split(s, '\n');
        for(String line : lines) {
            String[] v = StringUtils.split(line, '-');
            if (v.length != 2) continue;

            //Uwaga uwaga = uwagiDao.queryBuilder().where().eq("symbol", v[0]).and().eq("opis", v[1]).queryForFirst();
            Uwaga uwaga = getUwaga(v[0], v[1]);
            if (uwaga == null)
            {
                uwaga = new Uwaga(v[0], v[1]);
                uwagiDao.create(uwaga);
            }
            uwagi.add(uwaga);
        }

        parsujOdjazdy(Odjazd.DZIEN_ROBOCZY, x.l("/html/body/table[4]/tbody/tr[2]/td", url));
        parsujOdjazdy(Odjazd.DZIEN_SOBOTA, x.l("/html/body/table[4]/tbody/tr[3]/td", url));
        parsujOdjazdy(Odjazd.DZIEN_NIEDZIELA, x.l("/html/body/table[4]/tbody/tr[4]/td", url));
    }

    private Uwaga getUwaga(String symbol, String opis) throws SQLException {
        List<Uwaga> uwagi = uwagiDao.queryBuilder().where().eq("symbol", symbol).and().eq("opis", opis).query();
        if (uwagi.size() > 0) {
            return uwagi.get(0);
        }
        return null;
    }

    void parsujOdjazdy(int dzien, NodeList row) throws SQLException {
        for (int h = 0; h < 24; h++) {
            Node n = row.item(h + 1);
            for (String s : StringUtils.split(n.getTextContent())) {
                s = XPathHelper.fixUTF8(s);
                if(StringUtils.isNotBlank(s)) {
                    parsujOdjazd(dzien, h, s);
                }
            }
        }
    }

    private void parsujOdjazd(int dzien, int godzina, String s) throws SQLException {
        List<String> symbole = new ArrayList<String>();
        String m = "";
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (Character.isDigit(c))
                m += c;
            else
                symbole.add("" + c);
        }

        Odjazd odjazd = new Odjazd(rozklad, dzien, godzina, Integer.parseInt(m), StringUtils.join(symbole, ' '));
        odjazdyDao.create(odjazd);

        for (String symbol : symbole) {
            for (Uwaga uwaga : uwagi) {
                if (uwaga.symbol.equals(symbol)) {
                    odjazdUwagaDao.create(new OdjazdUwaga(odjazd, uwaga));
                    break;
                }
            }
        }
    }
}
