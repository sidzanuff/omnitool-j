package com.circumsolar.mzkbb;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

@DatabaseTable
public class Przystanek {

    public static final String COLUMN_NAZWA = "nazwa";

    @DatabaseField(id = true)
    public int id;
    @DatabaseField(columnName = COLUMN_NAZWA)
    public String nazwa;
    @ForeignCollectionField
    public Collection<Rozklad> rozklady;

    public Przystanek(){}

    public Przystanek(int id, String nazwa) {
        this.id = id;
        this.nazwa = nazwa;
    }
}
