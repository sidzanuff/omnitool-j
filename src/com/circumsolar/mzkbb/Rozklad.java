package com.circumsolar.mzkbb;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

@DatabaseTable
public class Rozklad {

    public static final String COLUMN_KURS_ID = "kurs_id";
    public static final String COLUMN_PRZYSTANEK_ID = "przystanek_id";
    public static final String COLUMN_NUMER = "numer";
    public static final String COLUMN_DODATKOWY = "dodatkowy";

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField(canBeNull = false, foreign = true, columnName = COLUMN_KURS_ID)
    public Kurs kurs;
    @DatabaseField(canBeNull = false, foreign = true, columnName = COLUMN_PRZYSTANEK_ID)
    public Przystanek przystanek;
    @DatabaseField(columnName = COLUMN_NUMER)
    public int numer;
    @DatabaseField(columnName = COLUMN_DODATKOWY)
    public boolean dodatkowy;
    @ForeignCollectionField
    public Collection<Odjazd> odjazdy;

    public Rozklad(){}

    public Rozklad(Kurs kurs, Przystanek przystanek, int numer, boolean dodatkowy) {
        this.kurs = kurs;
        this.przystanek = przystanek;
        this.numer = numer;
        this.dodatkowy = dodatkowy;
    }
}
