package com.circumsolar.mzkbb;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Uwaga {

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    public String symbol;
    @DatabaseField
    public String opis;

    public Uwaga(){}

    public Uwaga(String symbol, String opis) {
        this.symbol = symbol;
        this.opis = opis;
    }
}
