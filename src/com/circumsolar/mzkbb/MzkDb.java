package com.circumsolar.mzkbb;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

public class MzkDb {

    private static final String DB_SCHEMA = "jdbc:sqlite:";

    public String fileName;

    public Dao<Kurs, Integer> kursyDao;
    public Dao<Odjazd, Integer> odjazdyDao;
    public Dao<Przystanek, Integer> przystankiDao;
    public Dao<Rozklad, Integer> rozkladyDao;
    public Dao<Uwaga, Integer> uwagiDao;
    public Dao<OdjazdUwaga, Integer> odjazdUwagaDao;

    private ConnectionSource conn;

    public static MzkDb create(String fileName) throws SQLException {

        MzkDb db = new MzkDb();
        db.fileName = fileName;

        ConnectionSource conn = new JdbcConnectionSource(DB_SCHEMA + fileName);
        db.conn = conn;

        db.kursyDao = DaoManager.createDao(conn, Kurs.class);
        db.odjazdyDao = DaoManager.createDao(conn, Odjazd.class);
        db.przystankiDao = DaoManager.createDao(conn, Przystanek.class);
        db.rozkladyDao = DaoManager.createDao(conn, Rozklad.class);
        db.uwagiDao = DaoManager.createDao(conn, Uwaga.class);
        db.odjazdUwagaDao = DaoManager.createDao(conn, OdjazdUwaga.class);

        return db;
    }

    public void close() throws SQLException {
        conn.close();
    }
}
