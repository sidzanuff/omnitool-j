package com.circumsolar.mzkbb;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

@DatabaseTable
public class Odjazd {

    public static final String COLUMN_ROZKLAD_ID = "rozklad_id";
    public static final String COLUMN_DZIEN = "dzien";
    public static final String COLUMN_GODZINA = "godzina";
    public static final String COLUMN_MINUTA = "minuta";

    public static final int DZIEN_ROBOCZY = 0;
    public static final int DZIEN_SOBOTA = 1;
    public static final int DZIEN_NIEDZIELA = 2;

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    public int godzina;
    @DatabaseField
    public int minuta;
    @DatabaseField(columnName = COLUMN_DZIEN)
    public int dzien;
    @DatabaseField(canBeNull = false, foreign = true, columnName = COLUMN_ROZKLAD_ID)
    public Rozklad rozklad;
    @ForeignCollectionField
    public Collection<OdjazdUwaga> uwagi;
	@DatabaseField
	public String symbole;

    public Odjazd(){}

    public Odjazd(Rozklad rozklad, int dzien, int godzina, int minuta, String symbole) {
        this.rozklad = rozklad;
        this.dzien = dzien;
        this.godzina = godzina;
        this.minuta = minuta;
        this.symbole = symbole;
    }
}
