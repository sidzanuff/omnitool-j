package com.circumsolar.mzkbb;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Kurs {

    public static final String COLUMN_LINIA = "linia";
    public static final String COLUMN_KIERUNEK = "kierunek";

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField(columnName = COLUMN_LINIA)
    public String linia;
    @DatabaseField(columnName = COLUMN_KIERUNEK)
    public String kierunek;

    public Kurs(){}

    public Kurs(String linia, String kierunek) {
        this.linia = linia;
        this.kierunek = kierunek;
    }
}
