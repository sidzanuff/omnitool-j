package com.circumsolar.mzkbb;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class OdjazdUwaga {

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField(foreign =  true, canBeNull = false)
    public Odjazd odjazd;
    @DatabaseField(foreign =  true, canBeNull = false)
    public Uwaga uwaga;

    public OdjazdUwaga(){}

    public OdjazdUwaga(Odjazd odjazd, Uwaga uwaga) {
        this.odjazd = odjazd;
        this.uwaga = uwaga;
    }
}
